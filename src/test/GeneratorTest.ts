import { Generator } from "../app/text-generation/Generator"
import { eq, matchingWordCount } from "./Utilities"

const gettysburg = "Four score and seven years ago our fathers brought forth, on this continent, a new nation, " +
    "conceived in Liberty, and dedicated to the proposition that all men are created equal. Now we are engaged in a " +
    "great civil war, testing whether that nation, or any nation so conceived, and so dedicated, can long endure. " +
    "We are met on a great battle-field of that war. We have come to dedicate a portion of that field, as a final " +
    "resting-place for those who here gave their lives, that that nation might live. It is altogether fitting " +
    "and proper that we should do this. But, in a larger sense, we can not dedicate, we can not consecrate we can " +
    "not hallow this ground. The brave men, living and dead, who struggled here, have consecrated it far above our " +
    "poor power to add or detract. The world will little note, nor long remember what we say here, but it can " +
    "never forget what they did here. It is for us the living, rather, to be dedicated here to the unfinished work " +
    "which they who fought here have thus far so nobly advanced. It is rather for us to be here dedicated to the " +
    "great task remaining before us that from these honored dead we take increased devotion to that cause for which " +
    "they here gave the last full measure of devotion - that we here highly resolve that these dead shall not have " +
    "died in vain that this nation, under God, shall have a new birth of freedom, and that government of the people, " +
    "by the people, for the people, shall not perish from the earth."

const dream = "I am happy to join with you today in what will go down in history as the greatest demonstration for" +
    " freedom in the history of our nation. Five score years ago, a great American, in whose symbolic shadow we " +
    "stand today, signed the Emancipation Proclamation. This momentous decree came as a great beacon light of hope " +
    "to millions of Negro slaves who had been seared in the flames of withering injustice. It came as a joyous " +
    "daybreak to end the long night of their captivity. But one hundred years later, the Negro still is not free. " +
    "One hundred years later, the life of the Negro is still sadly crippled by the manacles of segregation and " +
    "the chains of discrimination. One hundred years later, the Negro lives on a lonely island of poverty in the " +
    "midst of a vast ocean of material prosperity. One hundred years later, the Negro is still languished in the " +
    "corners of American society and finds himself an exile in his own land. And so we've come here today to " +
    "dramatize a shameful condition. In a sense we've come to our nation's capital to cash a check. When the " +
    "architects of our republic wrote the magnificent words of the Constitution and the Declaration of Independence, " +
    "they were signing a promissory note to which every American was to fall heir. This note was a promise that all " +
    "men, yes, black men as well as white men, would be guaranteed the unalienable Rights of Life, Liberty and " +
    "the pursuit of Happiness. It is obvious today that America has defaulted on this promissory note, insofar as " +
    "her citizens of color are concerned. Instead of honoring this sacred obligation, America has given the Negro " +
    "people a bad check, a check which has come back marked \"insufficient funds.\""

const gettysburgWordCount = gettysburg.split(" ").length
const dreamWordCount = dream.split(" ").length

describe("Generator", () => {
    const seed = 100
    const stringSeed = "hello"

    describe("seed()", () => {

        it("supports chaining", () => {
            const generator = new Generator()
            eq(generator.seed(seed), generator)
        })

        it("takes an number or string as input", () => {
            const generator = new Generator()
                .seed(seed)
                .seed(stringSeed)
        })

        it("allows outputs to be reproduced", () => {
            const generator = new Generator()
                .seed(seed)
                .consume(gettysburg)
            const results: string[] = []
            for (let i = 0; i < 10; i++) {
                generator.seed(seed)
                results.push(generator.generateSentence())
            }
            for (const result of results.slice(1)) {
                eq(results[0], result)
            }
        })
    })

    describe("consume()", () => {
        it("supports chaining", () => {
            const generator = new Generator().seed(seed)
            eq(generator.consume(gettysburg), generator)
        })

        it("can take a single string or an array of strings as an argument", () => {
            const generator = new Generator().seed(seed)
                .consume(gettysburg)
                .consume([gettysburg, dream])
        })

        it("can take an integer weight to bias generation towards an input", () => {
            const generator = new Generator()
                .seed(seed)
                .consume(dream, 1000)
                .consume(gettysburg, 1)
            const output = generator.generateSentences(100)
            const countGettysburg = matchingWordCount(gettysburg, output)
            const countDream = matchingWordCount(dream, output)
            const ratio = countDream / countGettysburg

            // console.log("Matching word count ratio with weights 1000/1: " + ratio)
            // console.log("High bias word count: " + countDream)
            // console.log("Low bias word count: " + countGettysburg)

            expect(ratio).toBeGreaterThan(1.5)
        })

        it("makes base weights of long and short inputs equivalent", () => {
            const generator = new Generator()
            // .seed(seed)
                .consume(dream.repeat(100), 1)
                .consume(gettysburg, 1)
            const output = generator.generateSentences(2)
            const countGettysburg = matchingWordCount(gettysburg, output)
            const countDream = matchingWordCount(dream, output)
            const ratio = countDream / countGettysburg

            // console.log("Matching word count ratio with long/short: " + ratio)
            // console.log("Long input word count: " + countDream)
            // console.log("Short input word count: " + countGettysburg)

            expect(ratio).toBeLessThan(1.5)
            expect(ratio).toBeGreaterThan(0.5)
            console.log(output)
        })

        describe("performance", () => {
            const generator = new Generator().seed(seed)
            const text = gettysburg.repeat(500)
            it(`isn't terribly slow (${gettysburgWordCount * 500} words in < 1s)`, () => {
                generator.consume(text)
            }, 1000)
        })
    })

    describe("generateSentence() / generateSentences()", () => {
        it("returns an empty string if no text has been consumed yet", () => {
            const generator = new Generator().seed(seed)
            eq(generator.generateSentence(), "")
            eq(generator.generateSentences(10), "")
        })
        it("only allows a positive number of retries", () => {
            const generator = new Generator()
                .seed(seed)
                .consume(gettysburg)
            generator.generateSentence(0)
            generator.generateSentence(10)
            generator.generateSentences(10, 0)
            generator.generateSentences(10, 10)
            expect(() => generator.generateSentence(-1)).toThrowError()
            expect(() => generator.generateSentences(10, -1)).toThrowError()
        })

        describe("performance", () => {
            const generator = new Generator()
                .seed(seed)
                .consume(gettysburg.repeat(500))
            it("isn't terribly slow (5000 sentences in < 1s)", () => {
                generator.generateSentences(5000)
            }, 1000)
        })
    })

    describe("toString() / Internal Data Structure", () => {

        describe("uses single word states by default", () => {
            const generator = new Generator()
                .seed(seed)
                .consume(gettysburg)
            // console.log(generator.toString())
            // console.log(generator.generateSentences(2))
        })

        describe("can use arbitrary word sizes", () => {
            const generator = new Generator(() => 2)
                .seed(seed)
                .consume(gettysburg)
            // console.log(generator.toString())
            // console.log(generator.generateSentences(2))
        })

        describe("can use randomized word sizes", () => {
            const generator = new Generator(() => {
                return Math.random() > 0.6 ? 2 : 1
            })
                .seed(seed)
                .consume(gettysburg)
            // console.log(generator.toString())
            // console.log(generator.generateSentences(2))
        })
    })
})
