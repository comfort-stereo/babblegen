/**
 * Checks strict equality of two values.
 */
export function eq(actual: any, expected: any) {
    expect(actual).toBe(expected)
}

/**
 * Returns the number of occurences of all unique words from the first string in the second string.
 */
export function matchingWordCount(first: string, second: string): number {
    const wordsFirst = first.split(" ")
    const wordsSecond = second.split(" ")
    const wordMap = new Map()

    for (const word of wordsFirst) {
        if (wordMap.has(word)) {
            continue
        }
        wordMap.set(word, 0)
    }

    for (const word of wordsSecond) {
        if (wordMap.has(word)) {
            wordMap.set(word, wordMap.get(word) + 1)
        }
    }

    let sum = 0
    for (const count of wordMap.values()) {
        sum += count
    }

    return sum
}
