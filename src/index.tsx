import * as React from "react"
import * as ReactDOM from "react-dom"
import { View } from "./app/view/View"
import registerServiceWorker from "./registerServiceWorker"

import "bootswatch/yeti/bootstrap.min.css"
import "./app/style/index.css"

/**
 * Render the application.
 */
function render() {
    ReactDOM.render(<View/>, document.getElementById("root") as HTMLElement)
}

render()

/**
 * Enable hot reloading of React components on the development server.
 */
if (module.hot) {
    module.hot.accept("./app/view/View", () => {
        render()
    })
}

registerServiceWorker()
