import * as React from "react"
import { Component } from "react"
import { isNull, isUndefined } from "util"

interface TextViewProps {
    height: number
    editable?: boolean
    onTextChanged?: (text: string) => void
    defaultText?: string
    className?: string
}

/**
 * Component to edit or display medium to large amounts of text. Akin to a textarea element.
 */
export class TextView extends Component<TextViewProps> {

    private element: HTMLDivElement | null = null

    constructor(props: TextViewProps) {
        super(props)
    }

    render() {
        return (
            <div
                style={{height: this.props.height}}
                ref={
                    (element) => {
                        if (isNull(this.element)) {
                            this.element = element
                            if (!isNull(this.element) && !isUndefined(this.props.defaultText)) {
                                this.element.innerText = this.props.defaultText
                            }
                        }
                    }
                }
                onBlur={
                    /*
                     * Only emit a text changed event when the user has stopped editing.
                     */
                    (event) => {
                        const text = isNull(this.element) ? "" : this.element.innerText
                        if (!isUndefined(this.props.onTextChanged)) {
                            this.props.onTextChanged(text)
                        }
                    }
                }
                spellCheck={false}
                contentEditable={this.props.editable}
                className={"text-view" + " " + this.props.className || ""}
            >
                {this.props.children}
            </div>
        )
    }
}
