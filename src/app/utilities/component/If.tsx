import * as React from "react"

/**
 * Component that renders its children given a provided condition is true.
 */
export const If: React.SFC<{ condition: boolean }> = (props) => {
    if (props.condition) {
        return <span style={{display: props.condition ? null : "hidden"}}>
            {props.children}
        </span>
    }
    return <span/>
}
