import * as React from "react"

/**
 * Component used to insert horizontal or vertical space.
 */
export const Spacer: React.SFC<{ height?: number, width?: number }> = (props) => {
    if (typeof props.height !== "undefined") {
        return <div style={{height: props.height}}/>
    } else if (typeof props.width !== "undefined") {
        return <span style={{display: "inline-block", width: props.width}}/>
    }
    return <span/>
}
