import * as LoDash from "lodash"
import { LoDashStatic } from "lodash"
import { isUndefined } from "util"

/**
 * Utility functions extending LoDash.
 */
interface Utilities extends LoDashStatic {
    /**
     * Create a new array from an iterable with an index set to a new value.
     */
    set<T>(iterable: Iterable<T>, index: number, value: T): T[]

    /**
     * Return a generator that iterates over an array.
     */
    iterator<T>(array: T[]): IterableIterator<T>

    /**
     * Get a value from a Map with a given key, throwing an error if the key is not present. If the key is not present
     * and the otherwise parameter is provided, the otherwise parameter will be returned instead of throwing an error.
     */
    access<K, V>(map: Map<K, V>, key: K, otherwise?: V): V

    /**
     *
     */
    wait(ms?: number)

    /**
     *
     */
    run<T>(fn: () => T): T
}

const object = {
    set: function <T>(iterable: Iterable<T>, index: number, value: T): T[] {
        const result = Array.from(iterable)
        result[index] = value
        return result
    },

    iterator: function* iterator<T>(array: Iterable<T>): IterableIterator<T> {
        yield* array
    },

    access: function <K, V>(map: Map<K, V>, key: K, otherwise?: V): V {
        const result = map.get(key)
        if (result) {
            return result
        }
        if (isUndefined(otherwise)) {
            throw Error()
        }
        return otherwise
    },

    wait: async function (ms: number = 0): Promise<{}> {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve()
            }, ms)
        })
    },

    run: function <T>(fn: () => T): T {
        return fn()
    }
}

Object.setPrototypeOf(object, LoDash)

export const Ut = object as Utilities
