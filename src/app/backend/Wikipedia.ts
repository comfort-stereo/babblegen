import cheerio from "cheerio"

const headers = new Headers({
    "Api-User-Agent": "Babblegen/0.0.1 (https://babblegen.herokuapp.com)"
})

const minTextLength = 100

export async function getWikipediaPageURL(search: string): Promise<string> {
    const query = [
        "https://en.wikipedia.org/w/api.php",
        "?action=opensearch",
        "&search=" + search,
        "&limit=1",
        "&namespace=0",
        "&format=json",
        "&origin=*"
    ].join("")

    const response = await fetch(query, {method: "POST", headers: headers})
    const data = await response.json() as string[][]
    return data[1][0]
}

export async function getWikipediaContent(page: string): Promise<string> {
    const query = [
        "https://en.wikipedia.org/w/api.php",
        "?action=parse",
        "&page=" + page,
        "&format=json",
        "&origin=*"
    ].join("")

    const response = await fetch(query, {method: "POST", headers: headers})
    const json = await response.json()
    const parsed = json["parse"]
    const html = parsed["text"]["*"]
    const $ = cheerio.load(html)
    const paragraphs = $("p").map((i, element) => $(element).text()).toArray()
    const text = paragraphs.join(" ")

    if (text.length < minTextLength) {
        throw new Error("Article not found.")
    }

    return text
}
