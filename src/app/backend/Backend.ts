import { Configuration } from "../configuration/Configuration"
import { isNullOrUndefined } from "util"
import { getWikipediaContent, getWikipediaPageURL } from "./Wikipedia"

/**
 * Methods for communicating with the backend server.
 */
export namespace Backend {

    /**
     * Retrieves tweets from a given Twitter account.
     */
    export async function getTwitter(target: string): Promise<string[]> {
        const path = `api/twitter/${target}`
        return get(path)
    }

    /**
     * Retrieves posts from a given subreddit or Reddit account.
     */
    export async function getReddit(target: string, isSubreddit: boolean = false): Promise<string[]> {
        const path = `api/reddit/${isSubreddit ? "subreddit" : "user"}/${target}`
        return get(path)
    }

    /**
     * Retrieves body text from a Wikipedia article. This is should probably be done on the backend.
     */
    export async function getWikipedia(target: string): Promise<string[]> {
        const page = await getWikipediaPageURL(target)
        const text = await getWikipediaContent(page)
        return [text]
    }

    /**
     * Retrieves text data from the backend with a provided path.
     */
    async function get(path: string): Promise<string[]> {
        const query = Configuration.backendURL + path
        const response = await fetch(query)
        const json = await response.json()
        if (json instanceof Array && json.length > 0) {
            return json.filter((value) => !isNullOrUndefined(value))
        }
        throw new Error("Failed to fetch sources(s).")
    }
}