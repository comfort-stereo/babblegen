/**
 * References to resources used by the application.
 */
export namespace Resources {
    export const gear = require("../resources/gear.svg")
    export const twitterLogo = require("../resources/twitter.svg")
    export const redditLogo = require("../resources/reddit.svg")
    export const wikipediaLogo = require("../resources/wikipedia.svg")
    export const documentIconBlack = require("../resources/document-black.svg")
    export const documentIconWhite = require("../resources/document-white.svg")
    export const exitIcon = require("../resources/exit.svg")
    export const arrowIcon = require("../resources/arrow.svg")
    export const music = require("../resources/music.ogg")
}
