export namespace Configuration {
    /**
     * Name of the application, used for display purposes.
     */
    export const appName = "Babblegen"

    /**
     * URL of the proxy server used to retrieve data from website APIs.
     */
    export const backendURL = "https://babblegen-test.herokuapp.com"
}