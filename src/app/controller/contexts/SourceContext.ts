import { Source } from "../../source-types/Source"
import { RedditSource } from "../../source-types/RedditSource"
import { action, computed, observable } from "mobx"
import { Ut } from "../../utilities/Ut"

class Store {
    @observable sources: Source[] = [new RedditSource("askreddit")]
    @observable multiSourceMode: boolean = false
}

/**
 * Controller context that handles manipulation of input sources.
 */
export class SourceContext {

    private store = new Store()

    @computed
    get multiSourceMode(): boolean {
        return this.store.multiSourceMode
    }

    set multiSourceMode(on: boolean) {
        const sources = on ? this.store.sources : Ut.take(this.store.sources, 1)
        this.store.multiSourceMode = on
        this.store.sources = sources
    }

    get sources(): IterableIterator<Source> {
        return Ut.iterator(this.store.sources)
    }

    @computed
    get sourceCount(): number {
        return this.store.sources.length
    }

    get<T extends Source>(index: number): T {
        return this.store.sources[index] as T
    }

    @action
    put(index: number, source: Source) {
        this.store.sources[index] = source
    }

    @action
    apply(source: Source) {
        if (this.multiSourceMode) {
            this.store.sources.push(source)
        } else {
            this.store.sources = [source]
        }
    }

    @action
    remove(index: number) {
        Ut.pullAt(this.store.sources, index)
    }

    @action
    swap(first: number, second: number) {
        if (first < 0 || first >= this.sourceCount ||
            second < 0 || second >= this.sourceCount) {
            return
        }
        const temporary = this.get(first)
        this.store.sources[first] = this.get(second)
        this.store.sources[second] = temporary
    }
}
