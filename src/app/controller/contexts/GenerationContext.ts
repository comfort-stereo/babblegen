import { Generator } from "../../text-generation/Generator"
import { SourceContext } from "./SourceContext"
import { action, computed, observable, runInAction } from "mobx"
import { Source } from "../../source-types/Source"
import { Speaker } from "../../speech/Speaker"
import { Ut } from "../../utilities/Ut"

/**
 * State of the application in regard to text generation.
 */
export enum GenerationState {
    None = "none",
    Fetching = "fetching",
    Building = "building",
    Generating = "generating",
    Ready = "ready"
}

class Store {
    @observable state: GenerationState = GenerationState.None
    @observable output = ""
    @observable sentenceCount = 1
    @observable sentencesRemaining = 0
    @observable errorMessage: string | null = null
    @observable speak = false
}

/**
 * Controller context that handles actions and state involved in text generation.
 */
export class GenerationContext {

    static readonly minSentenceCountToAppearLoading = 50
    static readonly fetchDelay = 250
    static readonly buildDelay = 25
    static readonly doubleWordProbability = 0.65
    static readonly generationStride = 25

    private store = new Store()
    private source: SourceContext
    private generator: Generator | null = null
    private currentFetchID = 0

    constructor(source: SourceContext) {
        this.source = source
    }

    @computed
    get sentenceCount(): number {
        return this.store.sentenceCount
    }

    set sentenceCount(count: number) {
        this.store.sentenceCount = isNaN(count) ? count : Math.max(1, count) as number
    }

    @computed
    get generationProgress(): number {
        return Math.round((1 - this.store.sentencesRemaining / this.store.sentenceCount) * 100)
    }

    @computed
    get appearLoading(): boolean {
        if (this.store.state === GenerationState.Generating) {
            return this.store.sentenceCount >= GenerationContext.minSentenceCountToAppearLoading
        }
        return this.store.state !== GenerationState.None && this.store.state !== GenerationState.Ready
    }

    @computed
    get active(): boolean {
        return this.store.state !== GenerationState.None
    }

    @computed
    get state(): GenerationState {
        return this.store.state
    }

    @computed
    get output(): string {
        return this.store.output
    }

    @computed
    get errorMessage(): string | null {
        return this.store.errorMessage
    }

    @action
    clearError() {
        this.store.errorMessage = null
    }

    @computed
    get speak(): boolean {
        return this.store.speak
    }

    set speak(value: boolean) {
        this.store.speak = value
        if (this.store.speak && this.store.state === GenerationState.Ready) {
            Speaker.speak(this.store.output)
        } else {
            Speaker.stop()
        }
    }

    @action
    generate() {
        if (this.generator === null) {
            return
        }
        this.store.sentenceCount = Math.max(this.store.sentenceCount, 1)
        this.store.state = GenerationState.Generating
        this.store.sentencesRemaining = this.store.sentenceCount

        const sentences: string[] = []
        const generator = this.generator

        runInAction(async () => {

            /* Use to avoid creating a new lambda object for every generated sentence. */
            const iteration = action(async () => {
                sentences.push(generator.generateSentence())
                --this.store.sentencesRemaining
            })

            /* Main loop. */
            while (this.store.sentencesRemaining > 0 && this.active) {
                iteration()
                /* Yield for the UI to render and inputs to be processed every so often. */
                if (this.store.sentencesRemaining % GenerationContext.generationStride === 0) {
                    await Ut.wait()
                }
            }

            /* Return if the modal has been exited. */
            if (!this.active) {
                return
            }

            /* After every await statement the rest of the code that modifies state must be put into another action. */
            runInAction(() => {
                this.store.sentencesRemaining = 0
                this.store.state = GenerationState.Ready
                this.store.output = sentences.join(" ")
                if (this.speak) {
                    Speaker.speak(this.store.output)
                }
            })
        })
    }

    @action
    start() {
        const id = ++this.currentFetchID
        this.store.state = GenerationState.Fetching
        setTimeout(async () => {
            try {
                const content = await this.fetch()
                if (this.store.state === GenerationState.Fetching && id === this.currentFetchID) {
                    this.build(content)
                }
            } catch (error) {
                console.log(error)
            }
        }, GenerationContext.fetchDelay)
    }

    @action
    cancel() {
        if (this.store.state === GenerationState.Generating) {
            this.store.state = GenerationState.Ready
            this.store.sentencesRemaining = 0
        }
    }

    @action
    exit(clearError: boolean = false) {
        this.store.state = GenerationState.None
        this.store.sentencesRemaining = 0
        this.store.output = ""
        this.generator = null
        if (clearError) {
            this.clearError()
        }
        Speaker.stop()
    }

    @action
    private async fetch(): Promise<string[][]> {
        this.store.state = GenerationState.Fetching
        const sources = [...this.source.sources]
        const failed: Source[] = []

        const promises = sources.map((source) => {
            return source.load().catch(error => {
                failed.push(source)
                return []
            })
        })
        const results = await Promise.all(promises)

        if (failed.length > 0) {
            runInAction(() => {
                this.store.errorMessage = failed.map((source) => source.errorMessage).join("\n")
                this.exit()
            })
            throw new Error("Failed to fetch source(s).")
        }

        return results
    }

    @action
    private build(groups: string[][]) {
        this.store.state = GenerationState.Building
        setTimeout(action(() => {
            this.buildGenerator(groups)
            this.generate()
        }), GenerationContext.buildDelay)
    }

    @action
    private buildGenerator(groups: string[][]) {
        this.generator = new Generator(() => {
            if (Math.random() < GenerationContext.doubleWordProbability) {
                return 2
            }
            return 1
        })
        for (const group of groups) {
            this.generator.consume(group)
        }
    }
}
