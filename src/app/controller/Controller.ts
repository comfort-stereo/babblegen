import { GenerationContext } from "./contexts/GenerationContext"
import { SourceContext } from "./contexts/SourceContext"
import { useStrict } from "mobx"

/**
 * Singleton that contains the application's actions and state. Divided into contexts.
 */
export class Controller {
    static readonly instance = new Controller()

    readonly source: SourceContext = new SourceContext()
    readonly generation: GenerationContext = new GenerationContext(this.source)

    private constructor() {
        useStrict(true)
    }
}

/**
 * Function to access the controller globally.
 */
export function app() {
    return Controller.instance
}
