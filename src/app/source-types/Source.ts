export interface Source {
    /**
     * A number representing the influence of the source on generated text output.
     */
    weight: number

    /**
     * Error message displayed when the source cannot be retrieved.
     */
    errorMessage: string

    /**
     * Fetch the source, possibly asyncronously from its origin.
     */
    load(): Promise<string[]>
}
