import { ExternalSource } from "./ExternalSource"
import { Backend } from "../backend/Backend"
import { computed, observable } from "mobx"

class Store {
    @observable target: string
    @observable isSubreddit: boolean
    @observable weight: number
}

/**
 * Text generation source pointing to either a Reddit subreddit or account.
 */
export class RedditSource implements ExternalSource {

    private store = new Store()

    constructor(target: string = "", isSubreddit: boolean = true, weight: number = 1) {
        this.store.target = target
        this.store.isSubreddit = isSubreddit
        this.store.weight = weight
    }

    @computed
    get target(): string {
        return this.store.target
    }

    set target(value: string) {
        this.store.target = value
    }

    @computed
    get isSubreddit(): boolean {
        return this.store.isSubreddit
    }

    set isSubreddit(value: boolean) {
        this.store.isSubreddit = value
    }

    @computed
    get weight(): number {
        return this.store.weight
    }

    set weight(value: number) {
        this.store.weight = value
    }

    @computed
    get errorMessage(): string {
        const type = this.store.isSubreddit ? "subreddit" : "Reddit user"
        return `Failed to retrieve data from ${type} '${this.store.target}'.`
    }

    async load(): Promise<string[]> {
        return Backend.getReddit(this.store.target, this.store.isSubreddit)
    }
}
