import { Source } from "./Source"

/**
 * Interface for a source pointing to an 'username-like' target on a website.
 */
export interface ExternalSource extends Source {
    target: string
}
