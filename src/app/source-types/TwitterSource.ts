import { ExternalSource } from "./ExternalSource"
import { Backend } from "../backend/Backend"
import { computed, observable } from "mobx"

class Store {
    @observable target: string
    @observable weight: number
}

/**
 * Text generation source pointing to a Twitter account.
 */
export class TwitterSource implements ExternalSource {

    private store = new Store()

    constructor(user: string = "", weight: number = 1) {
        this.store.target = user
        this.store.weight = weight
    }

    @computed
    get target(): string {
        return this.store.target
    }

    set target(value: string) {
        this.store.target = value
    }

    @computed
    get weight(): number {
        return this.store.weight
    }

    set weight(value: number) {
        this.store.weight = value
    }

    @computed
    get errorMessage(): string {
        return `Failed to retrieve data from Twitter user '${this.store.target}'.`
    }

    async load(): Promise<string[]> {
        return Backend.getTwitter(this.store.target)
    }
}
