import { Source } from "./Source"
import { computed, observable } from "mobx"

class Store {
    @observable weight: number
    @observable text: string
}

/**
 * Simple text generation source that consists only of input text. Directly editable by the user.
 */
export class TextSource implements Source {

    private store = new Store()

    constructor(text: string = "", weight: number = 1) {
        this.store.text = text
        this.store.weight = weight
    }

    @computed
    get text(): string {
        return this.store.text
    }

    set text(value: string) {
        this.store.text = value
    }

    @computed
    get weight(): number {
        return this.store.weight
    }

    set weight(value: number) {
        this.store.weight = value
    }

    @computed
    get errorMessage(): string {
        return "Could not parse text source..."
    }

    async load(): Promise<string[]> {
        return [this.store.text]
    }
}
