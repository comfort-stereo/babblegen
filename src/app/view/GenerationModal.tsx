import "../style/index.css"
import * as React from "react"
import * as ControlLabel from "react-bootstrap/lib/ControlLabel"

import { Button, FormControl, Modal, ToggleButton, ToggleButtonGroup } from "react-bootstrap"
import { If } from "../utilities/component/If"
import { app } from "../controller/Controller"
import { Resources } from "../configuration/Resources"
import { TextView } from "../utilities/component/TextView"
import { Spacer } from "../utilities/component/Spacer"
import { UIUtility } from "../utilities/ReactUt"
import { observer } from "mobx-react"
import { GenerationState } from "../controller/contexts/GenerationContext"
import { Configuration } from "../configuration/Configuration"
import { Speaker } from "../speech/Speaker"

const minLoadingSentenceCount = 50

export const GenerationModal = observer(() =>
    <Modal show={true} backdrop={true} dialogClassName="generation-modal" onHide={
        () => app().generation.exit()
    }>
        <Header/>
        <Body height={UIUtility.onMobile ? 190 : 290}/>
        <hr/>
        <Controls/>
        <Footer/>
    </Modal>
)

const Header = observer(() =>
    <Modal.Header closeButton>
        <Modal.Title>
            {Configuration.appName}
        </Modal.Title>
    </Modal.Header>
)

const Body = observer((props: { height: number }) =>
    <div>
        {
            (() => {
                if (app().generation.appearLoading) {
                    return <LoadingIndicator height={props.height}/>
                } else {
                    return (
                        <TextView height={props.height}>
                            {app().generation.output}
                        </TextView>
                    )
                }
            })()
        }
    </div>
)

const LoadingIndicator = observer((props: { height: number }) =>
    <div style={{paddingTop: "10%", height: props.height}}>
        <img className="spinning-logo-loading" src={Resources.gear}/>
        <h4>
            {
                (() => {
                    switch (app().generation.state) {
                        case GenerationState.Fetching:
                            return "Fetching Sources"
                        case GenerationState.Building:
                            return "Building Generator"
                        case GenerationState.Generating:
                            return "Generating Text"
                        default:
                            return ""
                    }
                })()
            }
        </h4>
        <If condition={app().generation.state === GenerationState.Generating}>
            <Spacer height={20}/>
            <h4>
                {app().generation.generationProgress}%
            </h4>
        </If>
    </div>
)

const Controls = observer(() =>
    <div>
        <div>
            <ControlLabel>
                Sentence Count
            </ControlLabel>
            <div style={{display: "flex"}}>
                <div style={{width: "25%"}}/>
                <FormControl
                    style={{width: "50%"}}
                    type="number"
                    value={app().generation.sentenceCount}
                    disabled={app().generation.appearLoading}
                    onChange={(event) => app().generation.sentenceCount = UIUtility.getTargetValueNumber(event)}
                />
            </div>
        </div>

        <Spacer height={20}/>

        <div>
            <Button
                style={{width: "40%"}}
                bsSize="lg"
                bsStyle={
                    app().generation.state === GenerationState.Generating &&
                    app().generation.appearLoading ? "default" : "success"
                }
                disabled={
                    app().generation.appearLoading &&
                    app().generation.state !== GenerationState.Generating
                }
                onClick={() => {
                    if (app().generation.state === GenerationState.Generating) {
                        app().generation.cancel()
                    } else {
                        app().generation.generate()
                    }
                }}>
                {
                    app().generation.appearLoading &&
                    app().generation.state === GenerationState.Generating ? "Cancel" : "Generate"
                }
            </Button>
        </div>

        <Spacer height={20}/>

        <div>
            {UIUtility.onMobile ? null : <Spacer width={10}/>}
            <If condition={Speaker.available}>
                <ToggleButtonGroup type="checkbox" value={app().generation.speak ? [0] : []}>
                    <ToggleButton value={0} style={{width: 120}} onChange={
                        (event) => app().generation.speak = UIUtility.getTargetChecked(event)
                    }>
                        Speaker {app().generation.speak ? "On" : "Off"}
                    </ToggleButton>
                </ToggleButtonGroup>
            </If>
        </div>

        <Spacer height={20}/>
    </div>
)

const Footer = observer(() =>
    <Modal.Footer/>
)
