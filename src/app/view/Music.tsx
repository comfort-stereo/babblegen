import * as React from "react"
import { Resources } from "../configuration/Resources"
import { ControlLabel } from "react-bootstrap"
import { Spacer } from "../utilities/component/Spacer"

export const Music: () => JSX.Element = () =>
    <div>
        <ControlLabel>
            Mood Music?
        </ControlLabel>
        <Spacer height={5}/>
        <audio id="music" loop controls>
            <source src={Resources.music} type="audio/ogg"/>
        </audio>
    </div>
