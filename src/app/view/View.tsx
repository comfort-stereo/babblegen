import { Col, Grid, Row } from "react-bootstrap"
import * as React from "react"
import { Component } from "react"
import { Resources } from "../configuration/Resources"
import { Main } from "./Main"
import Helmet from "react-helmet"
import { Configuration } from "../configuration/Configuration"
import { Speaker } from "../speech/Speaker"
import { observer } from "mobx-react"
import { Music } from "./Music"

/**
 * Main UI element of the application.
 */
@observer
export class View extends Component {

    componentDidMount() {
        Speaker.initialize()
    }

    render(): JSX.Element {
        return (
            <div className="app">
                <div>
                    <Helmet>
                        <title>{Configuration.appName}</title>
                    </Helmet>
                </div>
                <div>
                    <div className="header">
                        <img src={Resources.gear} className="spinning-logo"/>
                        <h2>{Configuration.appName}</h2>
                    </div>
                </div>
                <Grid fluid>
                    <Row>
                        <Col xs={0} md={2}/>
                        <Col xs={20} md={8}>
                            <Main/>
                        </Col>
                        <Col xs={0} md={2}/>
                    </Row>
                </Grid>
                <Music/>
            </div>
        )
    }
}
