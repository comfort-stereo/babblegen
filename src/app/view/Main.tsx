import "../style/index.css"

import * as React from "react"
import { GenerationModal } from "./GenerationModal"
import { SourceViews } from "./SourceViews"
import { Button, ButtonGroup, ControlLabel, Form, FormGroup, ToggleButton, ToggleButtonGroup } from "react-bootstrap"
import { UIUtility } from "../utilities/ReactUt"
import { app } from "../controller/Controller"
import { TextSource } from "../source-types/TextSource"
import { Resources } from "../configuration/Resources"
import { TwitterSource } from "../source-types/TwitterSource"
import { RedditSource } from "../source-types/RedditSource"
import { Spacer } from "../utilities/component/Spacer"
import { observer } from "mobx-react"
import { If } from "../utilities/component/If"
import { GenerationErrorModal } from "./GenerationErrorModal"
import { WikipediaSource } from "../source-types/WikipediaSource"

export const Main = observer(() =>
    <div className="main">
        <If condition={app().generation.errorMessage !== null}>
            <GenerationErrorModal/>
        </If>
        <If condition={app().generation.active && app().generation.errorMessage === null}>
            <GenerationModal/>
        </If>
        <SourceViews/>
        <UpperControls/>
        <LowerControls/>
    </div>
)

const UpperControls = observer(() =>
    <div>
        <ControlLabel style={{paddingBottom: 10}}>
            Source
        </ControlLabel>
        <SourceButtons iconStyle={{height: 20, paddingLeft: UIUtility.onMobile ? 0 : 10}}/>
    </div>
)

const SourceButtons = observer((props: { iconStyle?: object }) =>
    <ButtonGroup bsSize="lg" justified>
        <ButtonGroup>
            <Button data-toggle="tooltip" title="Add text source" onClick={
                () => app().source.apply(new TextSource())
            }>
                {UIUtility.onMobile ? "" : "Text"}
                <img src={Resources.documentIconBlack} style={props.iconStyle}/>
            </Button>
        </ButtonGroup>

        <ButtonGroup>
            <Button data-toggle="tooltip" title="Add source from a Twitter account" onClick={
                () => app().source.apply(new TwitterSource())
            }>
                {UIUtility.onMobile ? "" : "Twitter"}
                <img src={Resources.twitterLogo} style={props.iconStyle}/>
            </Button>
        </ButtonGroup>

        <ButtonGroup>
            <Button data-toggle="tooltip" title="Add source from a Reddit account or subreddit" onClick={
                () => app().source.apply(new RedditSource())
            }>
                {UIUtility.onMobile ? "" : "Reddit"}
                <img src={Resources.redditLogo} style={props.iconStyle}/>
            </Button>
        </ButtonGroup>

        <ButtonGroup>
            <Button data-toggle="tooltip" title="Add source from a Wikipedia article" onClick={
                () => app().source.apply(new WikipediaSource())
            }>
                {UIUtility.onMobile ? "" : "Wikipedia"}
                <img src={Resources.wikipediaLogo} style={props.iconStyle}/>
            </Button>
        </ButtonGroup>
    </ButtonGroup>
)

const LowerControls = observer(() =>
    <div>
        <FormGroup>
            <Form inline onSubmit={(event) => event.preventDefault()}>
                <hr/>
                <Button style={{width: "80%"}} bsSize="lg" bsStyle="success" onClick={
                    () => app().generation.start()
                }>
                    Generate
                </Button>
                <hr/>
                <LowerControlForms/>
            </Form>
        </FormGroup>
    </div>
)

const LowerControlForms = observer(() =>
    <div>
        <div>
            <Spacer width={10}/>
            <ToggleButtonGroup type="checkbox" value={app().source.multiSourceMode ? [0] : []}>
                <ToggleButton value={0} style={{width: 300}} onChange={
                    (event) => app().source.multiSourceMode = UIUtility.getTargetChecked(event)
                }>
                    Multi-Source Mode {app().source.multiSourceMode ? "On" : "Off"}
                </ToggleButton>
            </ToggleButtonGroup>
        </div>
    </div>
)
