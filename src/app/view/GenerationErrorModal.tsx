import * as React from "react"
import { ConfirmationModal } from "../utilities/component/ConfirmationDialog"
import { app } from "../controller/Controller"

export const GenerationErrorModal = () =>
    <ConfirmationModal
        title="Error"
        onPositive={() => app().generation.exit(true)}
        onHide={() => app().generation.exit(true)}>
        <h4>
            {app().generation.errorMessage}
        </h4>
    </ConfirmationModal>
