import "../style/index.css"
import * as React from "react"
import {
    Button,
    ButtonGroup,
    ControlLabel,
    DropdownButton,
    Form,
    FormControl,
    FormGroup,
    MenuItem
} from "react-bootstrap"
import { app } from "../controller/Controller"
import { TextSource } from "../source-types/TextSource"
import { TwitterSource } from "../source-types/TwitterSource"
import { RedditSource } from "../source-types/RedditSource"
import { Resources } from "../configuration/Resources"
import { If } from "../utilities/component/If"
import { TextView } from "../utilities/component/TextView"
import { Spacer } from "../utilities/component/Spacer"
import { UIUtility } from "../utilities/ReactUt"
import { observer } from "mobx-react"
import { ExternalSource } from "../source-types/ExternalSource"
import { Ut } from "../utilities/Ut"
import { WikipediaSource } from "../source-types/WikipediaSource"

export const SourceViews = observer(() =>
    <div>
        {
            Ut.range(0, app().source.sourceCount).map((index) =>
                <div>
                    <div className="expanding-source-view">
                        <SourceView index={index}/>
                    </div>
                    <hr/>
                </div>)
        }
    </div>
)

const SourceView = observer((props: { index: number }) => {
    const source = app().source.get(props.index)
    if (source instanceof TextSource) {
        return <TextSourceView index={props.index}/>
    } else if (source instanceof TwitterSource) {
        return <TwitterSourceView index={props.index}/>
    } else if (source instanceof RedditSource) {
        return <RedditSourceView index={props.index}/>
    } else if (source instanceof WikipediaSource) {
        return <WikipediaSourceView index={props.index}/>
    } else {
        throw new TypeError()
    }
})

const TextSourceView = observer((props: { index: number }) =>
    <span>
        <SourceViewIcon image={Resources.documentIconBlack}/>
        <If condition={app().source.multiSourceMode}>
            <MultiSourceModeControls index={props.index}/>
        </If>
        <TextView
            editable
            height={116}
            defaultText={app().source.get<TextSource>(props.index).text}
            onTextChanged={(text) => app().source.get<TextSource>(props.index).text = text}
        />
    </span>
)

const TwitterSourceView = observer((props: { index: number }) =>
    <div>
        <SourceViewIcon image={Resources.twitterLogo}/>
        <If condition={app().source.multiSourceMode}>
            <MultiSourceModeControls index={props.index}/>
        </If>
        <Spacer width={10}/>
        <FormGroup>
            <Form inline onSubmit={
                (event) => {
                    event.preventDefault()
                    app().generation.start()
                }
            }>
                <ControlLabel>
                    Username
                </ControlLabel>
                <Spacer width={10}/>
                <ExternalSourceTargetForm index={props.index} prefix="@"/>
            </Form>
        </FormGroup>
    </div>
)

const RedditSourceView = observer((props: { index: number }) =>
    <div>
        <SourceViewIcon image={Resources.redditLogo}/>
        <If condition={app().source.multiSourceMode}>
            <MultiSourceModeControls index={props.index}/>
        </If>
        <Spacer width={10}/>
        <FormGroup>
            <Form inline onSubmit={
                (event) => {
                    event.preventDefault()
                    app().generation.start()
                }
            }>
                <DropdownButton
                    title={app().source.get<RedditSource>(props.index).isSubreddit ? "Subreddit" : "User"}
                    bsStyle="success"
                    id="">
                    <MenuItem value="User" onSelect={
                        () => app().source.get<RedditSource>(props.index).isSubreddit = false
                    }>
                        User
                    </MenuItem>
                    <MenuItem value="Subreddit" onSelect={
                        () => app().source.get<RedditSource>(props.index).isSubreddit = true
                    }>
                        Subreddit
                    </MenuItem>
                </DropdownButton>
                {UIUtility.onMobile ? <Spacer height={10}/> : <Spacer width={4}/>}
                <ExternalSourceTargetForm
                    index={props.index}
                    prefix={app().source.get<RedditSource>(props.index).isSubreddit ? "r/" : "u/"}
                />
            </Form>
        </FormGroup>
    </div>
)

const WikipediaSourceView = observer((props: { index: number }) =>
    <div>
        <SourceViewIcon image={Resources.wikipediaLogo}/>
        <If condition={app().source.multiSourceMode}>
            <MultiSourceModeControls index={props.index}/>
        </If>
        <Spacer width={10}/>
        <FormGroup>
            <Form inline onSubmit={
                (event) => {
                    event.preventDefault()
                    app().generation.start()
                }
            }>
                <ControlLabel>
                    Article
                </ControlLabel>
                <Spacer width={10}/>
                <ExternalSourceTargetForm index={props.index}/>
            </Form>
        </FormGroup>
    </div>
)

const ExternalSourceTargetForm = observer((props: { index: number, prefix?: string }) =>
    <FormControl
        value={(props.prefix || "") + app().source.get<ExternalSource>(props.index).target}
        type="text"
        spellCheck={false}
        onChange={
            (event) => {
                const target = UIUtility.getTargetValue(event).slice((props.prefix || "").length)
                const source = app().source.get<ExternalSource>(props.index)
                source.target = target
            }
        }
    />
)

const SourceViewIcon = observer((props: { image: string }) =>
    <img src={props.image} style={{height: 50, paddingRight: 10, paddingBottom: 20}}/>
)

const MultiSourceModeControls = observer((props: { index: number }) =>
    <span style={{display: "flex", justifyContent: "center"}}>
        <FormGroup>
            <Form inline onSubmit={(event) => event.preventDefault()}>
                <SourceViewDropdownButton
                    index={props.index}
                    iconStyle={{height: 20, paddingRight: 10}}
                />
                <ButtonGroup bsSize="xs" id="">
                    <Button style={{width: 33}} onClick={() => app().source.swap(props.index, props.index - 1)}>
                        <img className="flipped" style={{height: 8}} src={Resources.arrowIcon}/>
                    </Button>
                    <Button style={{width: 33}} onClick={() => app().source.swap(props.index, props.index + 1)}>
                        <img style={{height: 8}} src={Resources.arrowIcon}/>
                    </Button>
                    <Button color={"white"} style={{width: 33}} onClick={() => app().source.remove(props.index)}>
                        <img style={{height: 8}} src={Resources.exitIcon}/>
                    </Button>
                </ButtonGroup>
            </Form>
        </FormGroup>
    </span>
)

const SourceViewDropdownButton = observer((props: { index: number, iconStyle?: object }) =>
    <DropdownButton title="" bsSize="xs" bsStyle="success" style={{width: 100}} id="">
        <MenuItem onClick={() => app().source.put(props.index, new TextSource())}>
            <img src={Resources.documentIconBlack} style={props.iconStyle}/>
            Change To Text
        </MenuItem>
        <MenuItem onClick={() => app().source.put(props.index, new TwitterSource())}>
            <img src={Resources.twitterLogo} style={props.iconStyle}/>
            Change To Twitter
        </MenuItem>
        <MenuItem onClick={() => app().source.put(props.index, new RedditSource())}>
            <img src={Resources.redditLogo} style={props.iconStyle}/>
            Change To Reddit
        </MenuItem>
        <MenuItem onClick={() => app().source.put(props.index, new WikipediaSource())}>
            <img src={Resources.wikipediaLogo} style={props.iconStyle}/>
            Change To Wikipedia
        </MenuItem>
    </DropdownButton>
)
