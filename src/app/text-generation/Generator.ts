import * as Random from "random-seed"
import { State } from "./State"
import { isUndefined } from "util"
import { Ut } from "../utilities/Ut"

/**
 * Randomized text generator that consumes input text and generates output based on it. Uses Markov chains.
 */
export class Generator {
    /**
     * Whole words to be removed from input text before processing.
     */
    private static readonly ignored = new Set<string>(["&amp", ""])

    /**
     * Substrings signaling it's containing word should be removed before processing.
     */
    private static readonly includesIgnored = ["https://", "http://", "&amp", "&gt"]

    /**
     * Regex for all characters that should be removed from input text before processing.
     */
    private static readonly generalRemoved = /[()\[\]"*^~]|-+/g
    private static readonly wikipediaTag = /\[.*?]/g

    /**
     * Characters signaling the end of a sentence.
     */
    private static readonly endings = [".", "?", "!"]

    /**
     * Seedable random number generator.
     */
    private readonly randomizer = Random.create()

    /**
     * The start state of the Markov model.
     */
    private readonly start = new State()

    /**
     * All states in the model, keyed by word.
     */
    private readonly states = new Map<string, State>()

    /**
     * Function used to pick a state (word) size. Can be used to pick random state sizes, or pick them conditionally.
     */
    private readonly chooseStateSize: () => number

    constructor(stateSizeChooser: () => number = () => 1) {
        this.chooseStateSize = stateSizeChooser
        this.seed(Math.random())
    }

    /**
     * Returns true if a word ends a sentence.
     */
    private static isSentenceEnd(word: string): boolean {
        for (const end of Generator.endings) {
            if (word.includes(end)) {
                return true
            }
        }
        return false
    }

    /**
     * Adds a period to a string if the string does already end with a sentence-ending punctuation.
     */
    private static ensurePunctuation(text: string): string {
        if (text.length !== 0) {
            const punctuation = text[text.length - 1]
            if (!Ut.includes(Generator.endings, punctuation)) {
                return text + "."
            }
        }
        return text
    }

    /**
     * Removes unwanted words and/or bits of input words from text input.
     */
    private static clean(words: string[]): string[] {
        const cleaned: string[] = []

        for (let word of words) {
            let ignore = false
            word = word.replace(Generator.wikipediaTag, "")
            word = word.replace(Generator.generalRemoved, "")
            for (const tag of Generator.includesIgnored) {
                if (word.includes(tag)) {
                    ignore = true
                    break
                }
            }
            if (ignore || Generator.ignored.has(word)) {
                continue
            }
            cleaned.push(word)
        }

        return cleaned
    }

    /**
     * Joins single-word states into multi-word-states according to the choose state size function.
     */
    private static join(words: string[], chooseStateSize: () => number) {
        const groups: string[] = []

        let i = 0
        while (i < words.length) {
            let size = chooseStateSize()
            let end = Math.min(i + size - 1, words.length - 1)
            for (let j = i; j <= end; j++) {
                if (Generator.isSentenceEnd(words[j])) {
                    end = j
                }
            }
            const word = words.slice(i, end + 1).join(" ")
            groups.push(word)
            i += end - i + 1
        }

        return groups
    }

    private static capitalize(text: string): string {
        if (text.length === 0) {
            return ""
        }
        return text.charAt(0).toUpperCase() + text.slice(1)
    }

    /**
     * Seeds the generator so outputs can be reproduced.
     */
    seed(seed: number | string): Generator {
        this.randomizer.seed(seed.toString())
        return this
    }

    /**
     * Consumes input text as a string or an array of strings to be incorporated into future text generation. All inputs
     * have the same net influence on the output. Smaller inputs have a higher weight per-word ratio than larger inputs.
     */
    consume(texts: string | string[], weight: number = 1): Generator {
        if (texts instanceof Array) {
            texts = [...texts]
            for (let i = 0; i < texts.length; i++) {
                texts[i] = Generator.ensurePunctuation(texts[i])
            }
            texts = texts.join(" ")
        }

        const cleaned = Generator.clean(texts.split(/\s+/))
        const words = Generator.join(cleaned, this.chooseStateSize)
        const last = words.length - 1

        /* Give all single inputs a total weight of 1. */
        weight = weight / words.length

        let startedSentence = true

        for (let i = 0; i < words.length; i++) {
            const word = words[i]
            const state = this.getOrCreateState(word)

            if (startedSentence) {
                this.start.transition(word, weight)
                startedSentence = false
            }

            if (i !== last) {
                const next = words[i + 1]
                if (Generator.isSentenceEnd(word)) {
                    startedSentence = true
                } else {
                    state.transition(next, weight)
                }
            }
        }

        return this
    }

    /**
     * Generate a given number of sentences based on the current state of the Markov model.
     */
    generateSentences(count: number, retries: number = 20): string {
        const sentences: string[] = []
        for (let i = 0; i < count; i++) {
            const sentence = this.generateSentence(retries)
            if (sentence.length !== 0) {
                sentences.push(sentence)
            }
        }
        return sentences.join(" ")
    }

    /**
     * Generate a sentence based on the current state of the Markov model.
     */
    generateSentence(retries: number = 20): string {
        if (retries < 0) {
            throw Error("A negative number of retries was passed to generateSentence().")
        }

        const first = this.chooseNext(this.start)
        if (first === null) {
            return ""
        }

        const words = [Generator.capitalize(first)]
        let current = Ut.access(this.states, first)

        while (true) {
            const word = this.chooseNext(current)
            if (word === null) {
                break
            }
            words.push(word)
            current = Ut.access(this.states, word)
        }

        if (words.length !== 0) {
            const last = words[words.length - 1]
            words[words.length - 1] = Generator.ensurePunctuation(last)
        }

        const sentence = words.join(" ")
        if (sentence.length <= 1 && retries > 0) {
            return this.generateSentence(retries - 1)
        }

        return sentence
    }

    /**
     * Returns text showing the current state of the generator.
     */
    toString(): string {
        const object = {}
        for (const [word, state] of this.states.entries()) {
            object[word] = state.asJSON()
        }
        return JSON.stringify(object, null, 4)
    }

    /**
     * Randomly chooses the next state from a current state by probability.
     */
    private chooseNext(state: State): string | null {
        if (state.isEnd) {
            return null
        }
        const random = this.randomizer.random()
        const last = state.size - 1
        const sum = state.sum()

        let cumulative = 0

        for (const [word, weight] of state.transitions) {
            cumulative += weight / sum
            if (cumulative >= random) {
                return word
            }
        }

        return null
    }

    /**
     * Gets a state from the model by word. If the state is not present in the model, it will be added and returned.
     */
    private getOrCreateState(word: string): State {
        let state = this.states.get(word)
        if (isUndefined(state)) {
            state = new State()
            this.states.set(word, state)
        }
        return state
    }
}
