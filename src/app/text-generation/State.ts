import { isUndefined } from "util"

export class State {
    private readonly map = new Map<string, number>()
    private sumModified = false
    private sumCache = 0

    /**
     * Returns the number of transitions the state has.
     */
    get size(): number {
        return this.map.size
    }

    /**
     * Returns true if the state has no transitions.
     */
    get isEnd(): boolean {
        return this.size === 0
    }

    /**
     * Return an iterator over the state's transitions of the form [word, weight].
     */
    get transitions(): IterableIterator<[string, number]> {
        return this.map.entries()
    }

    /**
     * Returns net weight of all the state's transitions.
     */
    sum(): number {
        if (this.sumModified) {
            let sum = 0
            for (const transition of this.map.entries()) {
                sum += transition[1]
            }
            this.sumCache = sum
        }
        this.sumModified = false
        return this.sumCache
    }

    /**
     * Adds a transition to the state with a given weight or increases the weight of the transition by the given
     * weight if it already exists.
     */
    transition(word: string, weight: number) {
        const current = this.map.get(word)
        if (isUndefined(current)) {
            this.map.set(word, weight)
        } else {
            this.map.set(word, current + weight)
        }
        this.sumModified = true
    }

    /**
     * Returns a JSON stringifiable object representing the data in the state.
     */
    asJSON(): object {
        const object = {}
        for (const [word, weight] of this.map) {
            object[word] = weight
        }
        return object
    }
}
