import { isNullOrUndefined } from "util"

export class Speaker {

    /**
     * Reference to the browser's speech synthesis API.
     */
    private static synthesis: SpeechSynthesis = window.speechSynthesis

    /**
     * Returns true if the speech synthesis is available.
     */
    static get available(): boolean {
        return !isNullOrUndefined(window["chrome"]) && !isNullOrUndefined(Speaker.synthesis)
    }

    /**
     * Called when the application starts to ensure that speech synthesis voices are immediately loaded.
     */
    static initialize() {
        if (Speaker.available) {
            Speaker.synthesis.getVoices()
        }
    }

    /**
     * Start speaking a message. Stops speaking any text previously being read.
     */
    static speak(message: string) {
        if (Speaker.available) {
            this.stop()
            const words = message.split(/[;,.]/)
            for (const chunk of words) {
                const utterance = new SpeechSynthesisUtterance(chunk)
                const voice = Speaker.getVoice()
                if (voice) {
                    console.log("Playing voice: " + voice.name)
                    utterance.voice = voice
                }
                utterance.volume = 1
                utterance.rate = 0.9
                utterance.pitch = 1
                Speaker.synthesis.speak(utterance)
            }
        }
    }

    /**
     * Stop speaking all queued text.
     */
    static stop() {
        if (Speaker.available) {
            Speaker.synthesis.cancel()
        }
    }

    static getVoice(): SpeechSynthesisVoice | null {
        const splitRegex = /[\s\-]+/
        const voices = Speaker.synthesis.getVoices()
        const find = (voices: SpeechSynthesisVoice[], ...keywords: string[]) => {
            for (const keyword of keywords) {
                const voice = voices.find((voice) => {
                    const words = voice.name.toLowerCase().split(splitRegex)
                    return words.some((word) => word === keyword)
                })
                if (voice) {
                    return voice
                }
            }
            return null
        }

        let voice =
            voices.find(voice => voice.name === "Google UK English Female") ||
            voices.find(voice => voice.name === "English United Kingdom") || null
        if (typeof voice === null) {
            voice = find(voices, "uk", "english", "kingdom", "en", "eng") || null
        }
        return voice
    }
}
